import cv2
import numpy as np
from HandTrackingModule import HandDetector  # 手不检测方法
import time
import autopy
import win32gui, win32process, psutil
from ctypes import cast, POINTER
import random

# 定义猜拳状态
guess=['Rock','Scissor','Paper']

# （1）导数视频数据
wScr, hScr = autopy.screen.size()  # 返回电脑屏幕的宽和高(1920.0, 1080.0)
wCam, hCam = 1280, 720  # 视频显示窗口的宽和高
pt1, pt2 = (100, 100), (1000, 550)  # 虚拟鼠标的移动范围，左上坐标pt1，右下坐标pt2

cap = cv2.VideoCapture(0,cv2.CAP_DSHOW)  # 0代表自己电脑的摄像头
cap.set(3, wCam)  # 设置显示框的宽度1280
cap.set(4, hCam)  # 设置显示框的高度720

pTime = 0  # 设置第一帧开始处理的起始时间
frame = 0  # 初始化累计帧数

prev_state = [1, 1, 1, 1, 1]  # 初始化上一帧状态
current_state = [1, 1, 1, 1, 1]  # 初始化当前正状态

# （2）接收手部检测方法
detector = HandDetector(mode=False,  # 视频流图像
                        maxHands=1,  # 最多检测一只手
                        detectionCon=0.8,  # 最小检测置信度
                        minTrackCon=0.5)  # 最小跟踪置信度

# （3）处理每一帧图像
while True:
    # 图片是否成功接收、img帧图像
    success, img = cap.read()
    # 翻转图像，使自身和摄像头中的自己呈镜像关系
    img = cv2.flip(img, flipCode=1)  # 1代表水平翻转，0代表竖直翻转
    # 在图像窗口上创建一个矩形框，在该区域内移动鼠标
    # cv2.rectangle(img, pt1, pt2, (0, 255, 255), 5)

    player,pc=-1,random.randint(0,2)

    # 引导玩家出拳，3秒倒计时后截取最后一帧作为出拳结果
    sTime=time.time()
    while True:
        success,img=cap.read()
        img = cv2.flip(img, flipCode=1)
        eTime=time.time()
        #绘制出拳引导框
        cv2.rectangle(img, pt1, pt2, (0, 255, 255), 5)
        cv2.putText(img, 'Counting: '+str(3-int(eTime-sTime)), (150, 70), cv2.FONT_HERSHEY_SIMPLEX, 3, (255,255,255),3)
        cv2.imshow('frame',img)
        if eTime-sTime>3:
            break
        if cv2.waitKey(1) & 0xFF == 27:  # 每帧滞留20毫秒后消失
            break

    # （4）手部关键点检测
    # 传入图像, 返回手部关键点的坐标信息(字典)，绘制关键点后的图像
    hands, img = detector.findHands(img, flipType=False, draw=True)  # 上面反转过了，这里就不用再翻转了
    # print("hands:", hands)
    # [{'lmList': [[889, 652, 0], [807, 613, -25], [753, 538, -39], [723, 475, -52], [684, 431, -66], [789, 432, -27],
    #              [762, 347, -56], [744, 295, -78], [727, 248, -95], [841, 426, -39], [835, 326, -65], [828, 260, -89],
    #              [820, 204, -106], [889, 445, -54], [894, 356, -85], [892, 295, -107], [889, 239, -123],
    #              [933, 483, -71], [957, 421, -101], [973, 376, -115], [986, 334, -124]], 'bbox': (684, 204, 302, 448),
    #   'center': (835, 428), 'type': 'Right'}]
    # 如果能检测到手那么就进行下一步
    if hands:

        # 获取手部信息hands中的21个关键点信息
        lmList = hands[0]['lmList']  # hands是由N个字典组成的列表，字典包括每只手的关键点信息,此处代表第0个手
        hand_center = hands[0]['center']
        drag_flag = 0
        # 获取食指指尖坐标，和中指指尖坐标
        x1, y1, z1 = lmList[8]  # 食指尖的关键点索引号为8
        x2, y2, z2 = lmList[12]  # 中指指尖索引12
        cx, cy, cz = (x1 + x2) // 2, (y1 + y2) // 2, (z1 + z2) // 2  # 计算食指和中指两指之间的中点坐标
        hand_cx, hand_cy = hand_center[0], hand_center[1]
        # （5）检查哪个手指是朝上的
        fingers = detector.fingersUp(hands[0])  # 传入
        print("fingers", fingers)  # 返回 [0,1,1,0,0] 代表 只有食指和中指竖起
        # 255, 0,255 淡紫
        # 0,255,255  淡蓝
        # 255,255,0  淡黄
        # 计算食指尖和中指尖之间的距离distance,绘制好了的图像img,指尖连线的信息info
        distance, info, img = detector.findDistance((x1, y1), (x2, y2), img)  # 会画圈
        # 记录当前手势状态
        current_state = fingers

        # 只有食指和中指竖起，就认为是伸出了剪刀
        if fingers[1] == 1 and fingers[2] == 1 and sum(fingers) == 2:

            player=1
            print("玩家：剪刀")

        # 五指紧握，认为是伸出了石头
        elif fingers == [0, 0, 0, 0, 0]:

            player=0
            print("玩家：石头")

        # 所有手指张开，则认为是伸出了布
        elif fingers == [1, 1, 1, 1, 1]:

            player=2
            print("玩家：布")

    if player==-1:
        print('Can\'t detect your hand')
        cv2.putText(img, 'Can\'t detect', (150, 150), cv2.FONT_HERSHEY_PLAIN, 3, (255, 255, 255), 3)
    elif player==pc:
        print('Draw')
        cv2.putText(img, 'Player: '+guess[player], (150, 150), cv2.FONT_HERSHEY_PLAIN, 3, (255, 255, 255), 3)
        cv2.putText(img, 'PC: '+guess[pc], (150, 250), cv2.FONT_HERSHEY_PLAIN, 3, (255, 255, 255), 3)
        cv2.putText(img, 'Draw', (150, 350), cv2.FONT_HERSHEY_PLAIN, 3, (255, 255, 255), 3)
    elif (player==0 and pc==1) or (player==1 and pc==2) or (player==2 and pc==0):
        print('Player Win')
        cv2.putText(img, 'Player: '+guess[player], (150, 150), cv2.FONT_HERSHEY_PLAIN, 3, (255, 255, 255), 3)
        cv2.putText(img, 'PC: '+guess[pc], (150, 250), cv2.FONT_HERSHEY_PLAIN, 3, (255, 255, 255), 3)
        cv2.putText(img, 'You win', (150, 350), cv2.FONT_HERSHEY_PLAIN, 3, (255, 255, 255), 3)
    else:
        print('Player Lose')
        cv2.putText(img, 'Player: '+guess[player], (150, 150), cv2.FONT_HERSHEY_PLAIN, 3, (255, 255, 255), 3)
        cv2.putText(img, 'PC: '+guess[pc], (150, 250), cv2.FONT_HERSHEY_PLAIN, 3, (255, 255, 255), 3)
        cv2.putText(img, 'You lose', (150, 350), cv2.FONT_HERSHEY_PLAIN, 3, (255, 255, 255), 3)

    # 显示图像，输入窗口名及图像数据
    cv2.imshow('frame', img)
    cv2.waitKey(2500)

# 释放视频资源
cap.release()
cv2.destroyAllWindows()

# if __name__ == '__main__':
